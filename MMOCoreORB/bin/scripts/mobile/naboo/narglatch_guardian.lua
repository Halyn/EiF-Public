narglatch_guardian = Creature:new {
	objectName = "@mob/creature_names:narglatch_guardian",
	socialGroup = "narglatch",
	faction = "",
	mobType = MOB_CARNIVORE,
	level = 25,
	chanceHit = 0.35,
	damageMin = 225,
	damageMax = 250,
	baseXp = 2822,
	baseHAM = 7300,
	baseHAMmax = 9300,
	armor = 0,
	resists = {125,125,20,-1,15,165,-1,-1,-1},
	meatType = "meat_carnivore",
	meatAmount = 60,
	hideType = "hide_bristley",
	hideAmount = 35,
	boneType = "bone_mammal",
	boneAmount = 35,
	milk = 0,
	tamingChance = 0.25,
	ferocity = 9,
	passengerCapacity = 1,
	passengerSeatString = "cat",
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = PACK + KILLER + STALKER,
	optionsBitmask = AIENABLED,
	diet = CARNIVORE,


	templates = {"object/mobile/narglatch_hue.iff"},
	controlDeviceTemplate = "object/intangible/pet/narglatch_hue.iff",
	scale = 1.15,
	hues = { 24, 25, 26, 27, 28, 29, 30, 31 },
	lootGroups = {},

	-- Primary and secondary weapon should be different types (rifle/carbine, carbine/pistol, rifle/unarmed, etc)
	-- Unarmed should be put on secondary unless the mobile doesn't use weapons, in which case "unarmed" should be put primary and "none" as secondary
	primaryWeapon = "unarmed",
	secondaryWeapon = "none",
	conversationTemplate = "",
	
	-- primaryAttacks and secondaryAttacks should be separate skill groups specific to the weapon type listed in primaryWeapon and secondaryWeapon
	-- Use merge() to merge groups in creatureskills.lua together. If a weapon is set to "none", set the attacks variable to empty brackets
	primaryAttacks = { {"intimidationattack",""}, {"stunattack",""} },
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(narglatch_guardian, "narglatch_guardian")
