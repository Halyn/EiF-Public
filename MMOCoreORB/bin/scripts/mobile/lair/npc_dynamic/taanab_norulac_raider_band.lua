taanab_norulac_raider_band = Lair:new {
	mobiles = {{"norulac_raider", 1},{"norulac_raider_captain", 1},{"norulac_raider_commando", 1},{"norulac_raider_lieutenant", 1},{"norulac_raider_thug", 1}},
	spawnLimit = 20,
	buildingsVeryEasy = {},
	buildingsEasy = {},
	buildingsMedium = {},
	buildingsHard = {},
	buildingsVeryHard = {},
	mobType = "npc",
	buildingType = "none"
}

addLairTemplate("taanab_norulac_raider_band", taanab_norulac_raider_band)
