norulac_raider_lieutenant = Creature:new {
	objectName = "@mob/creature_names:norulac_raider_lieutenant",
	randomNameType = NAME_GENERIC,
	randomNameTag = true,
	mobType = MOB_NPC,
	socialGroup = "norulac",
	faction = "norulac_raiders",
	level = 103,
	chanceHit = 1.70,
	damageMin = 650,
	damageMax = 1025,
	baseXp = 10000,
	baseHAM = 28000,
	baseHAMmax = 35000,
	armor = 2,
	resists = {55,55,60,40,70,75,55,45,55},
	meatType = "",
	meatAmount = 0,
	hideType = "",
	hideAmount = 0,
	boneType = "",
	boneAmount = 0,
	milk = 0,
	tamingChance = 0,
	ferocity = 0,
	pvpBitmask = AGGRESSIVE + ATTACKABLE + ENEMY,
	creatureBitmask = KILLER + PACK,
	optionsBitmask = AIENABLED,
	diet = HERBIVORE,

	templates = {"object/mobile/dressed_mercenary_elite_hum_f.iff",
		     "object/mobile/dressed_mercenary_elite_hum_m.iff",
	             "object/mobile/dressed_mercenary_elite_nikto_m.iff",
	             "object/mobile/dressed_mercenary_elite_wee_m.iff",
		
	},

	lootGroups = {
		{
			groups = {
				{group = "junk", chance = 4500000},
				{group = "pistols", chance = 1500000},
				{group = "rifles", chance = 1500000},
				{group = "carbines", chance = 1500000},
				{group = "melee_weapons", chance = 500000},
				{group = "clothing_attachments", chance = 500000}
			}
		}
	},


	-- Primary and secondary weapon should be different types (rifle/carbine, carbine/pistol, rifle/unarmed, etc)
	-- Unarmed should be put on secondary unless the mobile doesn't use weapons, in which case "unarmed" should be put primary and "none" as secondary
	primaryWeapon = "dark_trooper_weapons",
	secondaryWeapon = "unarmed",
	conversationTemplate = "",
	reactionStf = "@npc_reaction/slang",
	
	-- primaryAttacks and secondaryAttacks should be separate skill groups specific to the weapon type listed in primaryWeapon and secondaryWeapon
	-- Use merge() to merge groups in creatureskills.lua together. If a weapon is set to "none", set the attacks variable to empty brackets
	primaryAttacks = merge(bountyhuntermaster,marksmanmaster,brawlermaster,swordsmanmaster,pistoleermaster),
	secondaryAttacks = { }
}

CreatureTemplates:addCreatureTemplate(norulac_raider_lieutenant, "norulac_raider_lieutenant")
