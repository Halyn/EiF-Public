--EiF 2022

BreachShotCommand = {
        name = "breachshot",
	damageMultiplier = 4.0,
	speedMultiplier = 2.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 45,
  	accuracyBonus = 5,

	coneAngle = 30,
	coneAction = true,

	poolsToDamage = RANDOM_ATTRIBUTE,

	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "breachshot",

	weaponType = SHOTGUNWEAPON,

	range = -1,

	armorBreakStrength = 10,
	generalArmorBreak = true,
}

AddCommand(BreachShotCommand)
