--EiF 2022

SweepShotCommand = {
        name = "sweepshot",
	damageMultiplier = 6.0,
	speedMultiplier = 2.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 45,
  	accuracyBonus = 30,

        --cone angle isn't strictly documented, leaving it alone
	coneAngle = 60,
	coneAction = true,

	poolsToDamage = HEALTH_ATTRIBUTE,

	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "sweepshot",

	weaponType = SHOTGUNWEAPON,

	range = -1
}

AddCommand(SweepShotCommand)
