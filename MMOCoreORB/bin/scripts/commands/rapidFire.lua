--EiF 2020
--Please do not use without permission

RapidFireCommand = {
        name = "rapidfire",

	damageMultiplier = 3.25,
	speedMultiplier = 1.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 45,
	mindCostMultiplier = 0,
	accuracyBonus = 25,
	coneAngle = 15,
	coneAction = true,

	animation = "fire_7_single",
	animType = GENERATE_RANGED,

	combatSpam = "s_auto",

	poolsToDamage = HEALTH_ATTRIBUTE,

	weaponType = ROTARYWEAPON,

	range = -1
}

AddCommand(RapidFireCommand)
