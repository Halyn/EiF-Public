--EiF 2022

ChokeShotCommand = {
        name = "chokeshot",
	damageMultiplier = 7.0,
	speedMultiplier = 2.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 45,
  	accuracyBonus = 30,

	coneAngle = 15,
	coneAction = true,

	poolsToDamage = HEALTH_ATTRIBUTE,

	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "chokeshot",

	weaponType = SHOTGUNWEAPON,

	range = -1
}

AddCommand(ChokeShotCommand)
