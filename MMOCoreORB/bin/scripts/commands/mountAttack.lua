MountAttackCommand = {
        name = "mountattack",
	damageMultiplier = 1,
	speedMultiplier = 1,

	combatSpam = "attack",
	animation = "",

	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 0,
	forceCostMultiplier = 0,
	visMod = 25,

	range = -1,
	
	trails = NOTRAIL,

	poolsToDamage = HEALTH_ATTRIBUTE
}

AddCommand(FireCannonsCommand)

