--EiF 2022

LuckyStrikeCommand = {
        name = "luckystrike",

  damageMultiplier = 6.0,
	speedMultiplier = 2.0,
	healthCostMultiplier = 0,
	actionCostMultiplier = 0,
	mindCostMultiplier = 30,




	animation = "fire_1_special_single",
	animType = GENERATE_RANGED,

	combatSpam = "luckystrike",

	range = -1
}

AddCommand(LuckyStrikeCommand)
