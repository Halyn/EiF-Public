--EiF 2020
--Please do not use without permission

OverwhelmingFireCommand = {
        name = "overwhelmingfire",

	damageMultiplier = 3.75,
	speedMultiplier = 1.25,
	healthCostMultiplier = 0,
	actionCostMultiplier = 45,
	mindCostMultiplier = 0,
	accuracyBonus = 25,

	animation = "fire_area",
	animType = GENERATE_INTENSITY,

	combatSpam = "a_auto",

	coneAngle = 60,
	coneAction = true,

	stateEffects = {
	  StateEffect(
		DIZZY_EFFECT,
		{},
		{ "dizzy_defense", "resistance_states" },
		{ "jedi_state_defense", "resistance_states" },
		50,
		0,
		30
	  ),
	},

	poolsToDamage = HEALTH_ATTRIBUTE,

	weaponType = ROTARYWEAPON,

	range = -1
}

AddCommand(OverwhelmingFireCommand)
