--Copyright (C) 2022 <Abi>
-- These files were created for Empire in Flames but are made available on MtG. If you use them, give Abi a thanks in whatever credits you have on your site.
-- Слава Україні! русский корабль, иди нахуй



object_draft_schematic_furniture_furniture_lamp_table_s03_on = object_draft_schematic_furniture_shared_furniture_lamp_table_s03_on:new {

	templateType = DRAFTSCHEMATIC,

	customObjectName = "Table-top Lamp \'Naboo\'",

	craftingToolTab = 512, -- (See DraftSchematicObjectTemplate.h)
	complexity = 16,
	size = 1,
	factoryCrateSize = 10,
	factoryCrateType = "object/factory/factory_crate_furniture.iff",
   
	xpType = "crafting_structure_general",
	xp = 140,

	assemblySkill = "structure_assembly",
	experimentingSkill = "structure_experimentation",
	customizationSkill = "structure_customization",

	customizationOptions = {},
	customizationStringNames = {},
	customizationDefaults = {},

	ingredientTemplateNames = {"craft_furniture_ingredients_n", "craft_furniture_ingredients_n", "craft_furniture_ingredients_n"},
	ingredientTitleNames = {"lamp_body", "lamp_assembly", "shade"},
	ingredientSlotType = {0, 0, 0},
	resourceTypes = {"metal", "metal", "mineral"},
	resourceQuantities = {35, 15, 20},
	contribution = {100, 100, 100},

	targetTemplate = "object/tangible/furniture/all/frn_all_light_lamp_table_s03.iff",

	additionalTemplates = {
				"object/tangible/furniture/all/frn_all_light_lamp_table_blue_s03.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_table_green_s03.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_table_orange_s03.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_table_purple_s03.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_table_red_s03.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_table_yellow_s03.iff",
			}
}
ObjectTemplates:addTemplate(object_draft_schematic_furniture_furniture_lamp_table_s03_on, "object/draft_schematic/furniture/furniture_lamp_table_s03_on.iff")
