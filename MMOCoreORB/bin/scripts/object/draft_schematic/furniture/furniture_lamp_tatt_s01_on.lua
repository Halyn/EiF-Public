--Copyright (C) 2022 <Abi>
-- These files were created for Empire in Flames but are made available on MtG. If you use them, give Abi a thanks in whatever credits you have on your site.
-- Слава Україні! русский корабль, иди нахуй



object_draft_schematic_furniture_furniture_lamp_tatt_s01_on = object_draft_schematic_furniture_shared_furniture_lamp_tatt_s01_on:new {

	templateType = DRAFTSCHEMATIC,

	customObjectName = "Table-top Lamp \'Coruscant\'",

	craftingToolTab = 512, -- (See DraftSchematicObjectTemplate.h)
	complexity = 18,
	size = 1,
	factoryCrateSize = 10,
	factoryCrateType = "object/factory/factory_crate_furniture.iff",
   
	xpType = "crafting_structure_general",
	xp = 250,

	assemblySkill = "structure_assembly",
	experimentingSkill = "structure_experimentation",
	customizationSkill = "structure_customization",

	customizationOptions = {},
	customizationStringNames = {},
	customizationDefaults = {},

	ingredientTemplateNames = {"craft_furniture_ingredients_n", "craft_furniture_ingredients_n", "craft_furniture_ingredients_n", "craft_furniture_ingredients_n"},
	ingredientTitleNames = {"lamp_body", "neck", "lamp_assembly", "shade"},
	ingredientSlotType = {0, 0, 0, 0},
	resourceTypes = {"metal", "metal", "metal", "mineral"},
	resourceQuantities = {40, 50, 15, 20},
	contribution = {100, 100, 100, 100},

	targetTemplate = "object/tangible/furniture/all/frn_all_light_lamp_tatt_s01.iff",

	additionalTemplates = {
				"object/tangible/furniture/all/frn_all_light_lamp_tatt_blue_s01.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_tatt_green_s01.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_tatt_orange_s01.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_tatt_purple_s01.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_tatt_red_s01.iff",
				"object/tangible/furniture/all/frn_all_light_lamp_tatt_yellow_s01.iff",
			}
}
ObjectTemplates:addTemplate(object_draft_schematic_furniture_furniture_lamp_tatt_s01_on, "object/draft_schematic/furniture/furniture_lamp_tatt_s01_on.iff")
