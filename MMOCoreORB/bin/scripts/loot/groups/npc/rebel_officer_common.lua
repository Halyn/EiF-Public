rebel_officer_common = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "radar_screen_schematic", weight = 3300000},
		{itemTemplate = "technical_console_schematic_1", weight = 3300000},
		{itemTemplate = "technical_console_schematic_2", weight = 3300000},
		{groupTemplate = "rebel_books", weight = 100000},
	}
}

addLootGroupTemplate("rebel_officer_common", rebel_officer_common)
