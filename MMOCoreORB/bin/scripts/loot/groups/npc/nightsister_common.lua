nightsister_common = {
	description = "",
	minimumLevel = 0,
	maximumLevel = 0,
	lootItems = {
		{itemTemplate = "clothing_hat_nightsister_s01_schematic", weight = 312500},
		{itemTemplate = "clothing_hat_nightsister_s02_schematic", weight = 312500},
		{itemTemplate = "clothing_hat_nightsister_s03_schematic", weight = 312500},
		{itemTemplate = "nightsister_vibro_unit", weight = 1250000},
		{itemTemplate = "throw_pillow_schematic", weight = 1750000},
		{itemTemplate = "campfire_schematic", weight = 1750000},
		{itemTemplate = "necklace_primative_schematic", weight = 1750000},
		{itemTemplate = "pitcher_full_schematic", weight = 1750000},
		{groupTemplate = "nightsister_books", weight = 812500},
	}
}

addLootGroupTemplate("nightsister_common", nightsister_common)
