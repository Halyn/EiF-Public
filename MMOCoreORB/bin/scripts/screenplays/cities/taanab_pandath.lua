TaanabPandathScreenPlay = CityScreenPlay:new {
	numberOfActs = 1,

	screenplayName = "TaanabPandathScreenPlay",

	planet = "taanab",

	attackableMobiles = {	
	--streets
	-- these two criminals were commmented out in the original file, so leaving them as is?
	--	spawnMobile("taanab", "criminal",600,2150.6,45,5327.2,18,0)
	--	spawnMobile("taanab", "criminal",600,2150.6,45,5331.8,134,0)	
		{"thug",600,2116.8,45,5271.9,157,0,""},
		{"thug",600,2122.4,45,5270.2,-140,0,""},
		{"criminal",600,2088.9,45,5308.7,99,0,""},
		{"criminal",600,2100.2,45,5529.6,-7,0,""},
		{"thug",600,2097.9,45,5534.4,129,0,""},
		{"thief",600,1892,45,5525.4,-105,0,""},
		{"thief",600,1885.4,45,5518.2,-23,0,""},		
		{"thug",600,1887.5,45,5363.6,12,0,""},
		{"criminal",600,1877.2,45,5402.5,-53,0,""},	
		{"thug",600,1885.1,45,5371.1,114,0,""},


	},
		
	nonAttackableMobiles = {	
	--streets
		{"pilot",600,2135.6,45,5366.4,-69,0,""},
		{"commoner",600,2132.6,45,5366.1,78,0,""},
		{"pilot",600,2138.9,45,5375.2,4,0,""},
		{"farmer",600,2129.5,45,5392.9,-147,0,""},
		{"noble",600,2127.5,45,5366.4,-69,0,""},
		{"informant_npc_lvl_3",600,2006.3,45,5537.6,152,0,""},
		{"commoner",600,2009.9,45,5537.8,-122,0,""},
		{"r3",600,2099.2,45,5510.8,-171,0,""},
		{"commoner",600,2098,45,5507.9,20,0,""},
		{"commoner",600,2033.1,45,5370.8,-178,0,""},
		{"commoner",600,2031.9,45,5367.8,44,0,""},
		{"farmer",600,2123.6,45,5471.3,156,0,""},
		{"trainer_artisan",600,2010.1,45.5,5463,-44,0,""},
		{"info_broker",600,2139.5,45,5376.6,-134,0,""},
		{"trainer_scout",600,2169.2,45,5346.9,-104,0,""},
		{"trainer_ranger",600,2164.3,45,5350.5,-177,0,""},

		{"informant_npc_lvl_1",600,2089.8,45,5320.9,32,0,""},
		{"informant_npc_lvl_2",600,2076.8,45,5319.5,-61,0,""},
		{"commoner",600,1956.1,45,5464.0,-171,0,""},
		{"commoner",600,1957,45,5460.3,-57,0,""},
		{"commoner",600,1962.4,45,5458.6,10,0,""},
		{"commoner",600,1921.8,45,5420.4,168,0,""},

		{"farmer",600,1935.5,45,5389.6,98,0,""},
		{"noble",600,1921.3,45,5416,47,0,""},
		{"contractor",600,1899.3,47.5,5339.2,-11,0,""},
		{"scientist",600,1982.6,45,5287.5,50,0,""},
		{"trainer_creaturehandler",600,2169.4,45,5342.8,-90,0,""},
		{"commoner",600,2050,45,5365,75,0,""},
		{"commoner",600,2060,45,5368,-90,0,""},
		{"commoner",600,2049,45,5372,99,0,""},
		{"commoner",600,2048,45,5374,89,0,""},
		{"commoner",600,2059,45,5375,-100,0,""},
		{"commoner",600,2058,45,5385,-115,0,""},
		{"commoner",600,2059,45,5387,-90,0,""},
		{"commoner",600,2060,45,5396,-45,0,""},
		{"commoner",600,2050,45,5382,90,0,""},
		{"commoner",600,2049,45,5425,96,0,""},
		{"commoner",600,2049,45,5323,84,0,""},
		{"commoner",600,2057.7,45,5435.9,-155,0,""},
		{"commoner",600,2053.1,45,5431.5,124,0,""},
		{"commoner",600,2051,45,5408.4,113,0,""},
		{"commoner",600,2048.8,45,5422.9,60,0,""},
		{"commoner",600,2069.6,45,5412.3,-130,0,""},
		{"commoner",600,2071.6,45,5418.2,-152,0,""},
		{"commoner",600,2071.1,45,5415.1,-6,0,""},
		{"commoner",600,2051.6,45,5322.1,-67,0,""},
		{"commoner",600,2053.4,45,5391.3,97,0,""},
		{"pilot",600,2102.3,45,5373.1,154,0,""},
		{"pilot",600,2103.1,45,5369.8,-12,0,""},

		{"farmer",600,2099.8,45,5358.4,-47,0,""},
		{"r2",600,1912.1,45,5544.3,-43,0,""},

		{"commoner",600,1910.7,45,5466.9,153,0,""},
		{"commoner",600,1909.1,45,5463.8,38,0,""},

		--hotel
		{"bartender",600,20.1,1.6,13,164,6037380, ""},
		{"patron_devaronian",600,20.7,1.3,10.4,-62,6037380,""},
		{"patron_quarren",600,18.8,1.3,10.6,61,6037380,""},
		{"businessman",600,-23,1.6,10.7,122,6037381,""},

		--cantina
		{"bartender",600,8.5,-0.9,1,78,6036259,""},
		{"patron_chiss",600,11.3,-0.9,-1.3,-39,6036259,""},
		{"patron_ithorian",600,10.8,-0.9,2.9,-163,6036259,""},
		{"patron_ishitib",600,9.1,-0.9,10.1,84,6036259,""},
		{"patron",600,11.1,-0.9,11.3,-160,6036259,""},
		{"patron",600,22.3,-0.9,20.8,-147,6036261,""},
		{"patron",600,15.9,-0.9,18.2,99,6036261,""},
		{"entertainer",600,20.5,-0.9,-1.6,20,6036259,""},
		
		--guild halls
		{"trainer_merchant",600,11,1.1,5.5,162,6036340,""},
		{"trainer_weaponsmith",600,-2.9,1.1,-8.7,126,6036343,""},
		{"trainer_droidengineer",600,-14.6,1.1,-11.2,82,6036344,""},
		{"trainer_armorsmith",600,-15.2,1.1,0.1,91,6036341,""},
		{"trainer_tailor",600,-14.9,1.1,4.4,99,6036341,""},
		{"trainer_chef",600,-13.2,1.1,-13.5,17,6036134,""},
		{"trainer_architect",600,7,1.1,-11.7,84,6036132,""},
		{"trainer_artisan",600,3.3,1.1,-9.1,-106,6036133,""},

		--med center
		{"trainer_doctor",600,17.1,0.3,1.2,118,6036502,""},
		{"trainer_medic",600,22.1,0.3,-8,-9,6036502,""},
		{"trainer_surveyor",600,-21.4,0.3,-10.1,166,6036506,""},
		{"trainer_bioengineer",600,-24.4,0.3,1.3,161,6036506,""},
		
	
	},
}


registerScreenPlay("TaanabPandathScreenPlay", true)

function TaanabPandathScreenPlay:start()
	if (isZoneEnabled(self.planet)) then
		self:spawnMobiles()
		self:spawnJunkDealers()
		self:spawnNonAttackableMobiles()
		self:spawnAttackableMobiles()
		self:setupFreighters()
	end
end

function TaanabPandathScreenPlay:setupFreighters()
	if (isZoneEnabled("taanab")) then
		local pFreighter = spawnSceneObject("taanab", "object/mobile/animated_ykl37r.iff", 2043, 52.1, 5620, 0, 0)
		writeData("TaanabFreighter1", SceneObject(pFreighter):getObjectID())
		CreatureObject(pFreighter):setPosture(2)
		pFreighter = spawnSceneObject("taanab", "object/mobile/animated_yt1300.iff", 2043, 52.1, 5620, 0, 0)
		writeData("TaanabFreighter2", SceneObject(pFreighter):getObjectID())
		CreatureObject(pFreighter):setPosture(2)
		pFreighter = spawnSceneObject("taanab", "object/mobile/animated_yt2000.iff", 2043, 52.1, 5620, 0, 0)
		writeData("TaanabFreighter3", SceneObject(pFreighter):getObjectID())
		CreatureObject(pFreighter):setPosture(2)
		pFreighter = spawnSceneObject("taanab", "object/creature/npc/theme_park/player_transport.iff", 2043, 52.1, 5620, 0, 0)
		writeData("TaanabFreighter4", SceneObject(pFreighter):getObjectID())
		CreatureObject(pFreighter):setPosture(2)

		createEvent(90 * 1000, "TaanabPandathScreenPlay", "scheduleLanding", pFreighter, "")
	end
end


function TaanabPandathScreenPlay:scheduleTakeoff(pFreighter)
	createEvent(getRandomNumber(60, 300) * 1000, "TaanabPandathScreenPlay", "scheduleLanding", pFreighter, "")
	self:shipTakeoff(pFreighter)
end


function TaanabPandathScreenPlay:scheduleLanding(pNil)
	local nextLanding = getRandomNumber(1, 4)
	local freighterID = readData("TaanabFreighter" .. nextLanding)
	local pFreighter = getSceneObject(freighterID)
	self:shipLanding(pFreighter)
	createEvent(getRandomNumber(300, 600) * 1000, "TaanabPandathScreenPlay", "scheduleTakeoff", pFreighter, "")
end

function TaanabPandathScreenPlay:shipTakeoff(pShip)
	CreatureObject(pShip):setPosture(2)
end

function TaanabPandathScreenPlay:shipLanding(pShip)
	CreatureObject(pShip):setPosture(0)
end
