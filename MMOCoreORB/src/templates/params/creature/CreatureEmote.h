/*
 * CreatureEmote.h
 *
 *  Created on: Jan 28, 2014
 *      Author: Klivian
 */

#ifndef CREATUREEMOTE_H_
#define CREATUREEMOTE_H_

class CreatureEmote {
public:

	// Partial list
	enum {
		BAD      = 17,
		BECKON   = 26,
		BONK     = 39,
		HUG      = 168,
		NUZZLE   = 221,
		PET      = 230,
		POINTAT  = 238,
		REASSURE = 264,
		SCOLD    = 276,
		SLAP     = 291,
		SUMMON   = 324,
		TAP      = 333,
		WHAP     = 367
	};
};


#endif /* CREATUREEMOTE_H_ */
