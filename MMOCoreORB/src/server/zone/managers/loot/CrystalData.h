#ifndef CRYSTALDATA_H_
#define CRYSTALDATA_H_

#include "engine/engine.h"

class CrystalData : public Object {
	float minDamage;
	float maxDamage;
	int minHitpoints;
	int maxHitpoints;
	float minHealthSac;
	float maxHealthSac;
	float minActionSac;
	float maxActionSac;
	float minMindSac;
	float maxMindSac;
	float minAttackSpeed;
	float maxAttackSpeed;
	float minWoundChance;
	float maxWoundChance;
	float minIdealAccuracy;
	float maxIdealAccuracy;
	float minPointBlankAccuracy;
	float maxPointBlankAccuracy;

public:
	CrystalData() : Object(), minDamage(0), maxDamage(0), minHitpoints(0), maxHitpoints(0), minHealthSac(0), maxHealthSac(0), minActionSac(0), maxActionSac(0),
		minMindSac(0), maxMindSac(0), minAttackSpeed(0.0), maxAttackSpeed(0.0), minWoundChance(0), maxWoundChance(0), minIdealAccuracy(0), maxIdealAccuracy(0),minPointBlankAccuracy(0),maxPointBlankAccuracy(0) {
	}

	CrystalData(const CrystalData& data) : Object() {
		minDamage = data.minDamage;
		maxDamage = data.maxDamage;
		minHitpoints = data.minHitpoints;
		maxHitpoints = data.maxHitpoints;
		minHealthSac = data.minHealthSac;
		maxHealthSac = data.maxHealthSac;
		minActionSac = data.minActionSac;
		maxActionSac = data.maxActionSac;
		minMindSac = data.minMindSac;
		maxMindSac = data.maxMindSac;
		minAttackSpeed = data.minAttackSpeed;
		maxAttackSpeed = data.maxAttackSpeed;
		minWoundChance = data.minWoundChance;
		maxWoundChance = data.maxWoundChance;
		minIdealAccuracy = data.minIdealAccuracy;
	  	maxIdealAccuracy = data.maxIdealAccuracy;
		minPointBlankAccuracy = data.minPointBlankAccuracy;
		maxPointBlankAccuracy = data.maxPointBlankAccuracy;
	}

	CrystalData& operator=(const CrystalData& data) {
		if (this == &data)
			return *this;

		minDamage = data.minDamage;
		maxDamage = data.maxDamage;
		minHitpoints = data.minHitpoints;
		maxHitpoints = data.maxHitpoints;
		minHealthSac = data.minHealthSac;
		maxHealthSac = data.maxHealthSac;
		minActionSac = data.minActionSac;
		maxActionSac = data.maxActionSac;
		minMindSac = data.minMindSac;
		maxMindSac = data.maxMindSac;
		minAttackSpeed = data.minAttackSpeed;
		maxAttackSpeed = data.maxAttackSpeed;
		minIdealAccuracy = data.minIdealAccuracy;
	  	maxIdealAccuracy = data.maxIdealAccuracy;
		minPointBlankAccuracy = data.minPointBlankAccuracy;
		maxPointBlankAccuracy = data.maxPointBlankAccuracy;
		minWoundChance = data.minWoundChance;
		maxWoundChance = data.maxWoundChance;

		return *this;
	}

	void readObject(LuaObject* luaObject) {
		minDamage = luaObject->getIntField("minDamage");
		maxDamage = luaObject->getIntField("maxDamage");
		minHitpoints = luaObject->getIntField("minHitpoints");
		maxHitpoints = luaObject->getIntField("maxHitpoints");
		minHealthSac = luaObject->getIntField("minHealthSac");
		maxHealthSac = luaObject->getIntField("maxHealthSac");
		minActionSac = luaObject->getIntField("minActionSac");
		maxActionSac = luaObject->getIntField("maxActionSac");
		minMindSac = luaObject->getIntField("minMindSac");
		maxMindSac = luaObject->getIntField("maxMindSac");
		minAttackSpeed = luaObject->getFloatField("minAttackSpeed");
		maxAttackSpeed = luaObject->getFloatField("maxAttackSpeed");
		minWoundChance = luaObject->getIntField("minWoundChance");
		maxWoundChance = luaObject->getIntField("maxWoundChance");
		minIdealAccuracy = luaObject->getIntField("minIdealAccuracy");
	  	maxIdealAccuracy = luaObject->getIntField("minIdealAccuracy");
		minPointBlankAccuracy = luaObject->getIntField("minPointBlankAccuracy");
		maxPointBlankAccuracy =luaObject->getIntField("maxPointBlankAccuracy");
	}

	inline float getMinDamage() const {
		return minDamage;
	}

	inline float getMaxDamage() const {
		return maxDamage;
	}

	inline int getMinHitpoints() const {
		return minHitpoints;
	}

	inline int getMaxHitpoints() const {
		return maxHitpoints;
	}

	inline float getMinHealthSac() const {
		return minHealthSac;
	}

	inline float getMaxHealthSac() const {
		return maxHealthSac;
	}

	inline float getMinActionSac() const {
		return minActionSac;
	}

	inline float getMaxActionSac() const {
		return maxActionSac;
	}

	inline float getMinMindSac() const {
		return minMindSac;
	}

	inline float getMaxMindSac() const {
		return maxMindSac;
	}

	inline float getMinWoundChance() const {
		return minWoundChance;
	}

	inline float getMinAttackSpeed() const {
		return minAttackSpeed;
	}

	inline float getMaxAttackSpeed() const {
		return maxAttackSpeed;
	}

	inline float getMaxWoundChance() const {
		return maxWoundChance;
	}

	inline float getMinPointBlankAccuracy() const {
		return minPointBlankAccuracy;
	}

	inline float getMaxPointBlankAccuracy() const {
		return maxPointBlankAccuracy;
	}

	inline float getMinIdealAccuracy() const {
		return minIdealAccuracy;
	}

	inline float getMaxIdealAccuracy() const {
		return maxIdealAccuracy;
	}
};

#endif /* CRYSTALDATA_H_ */
