/*
 * WeatherManagerImplementation.cpp
 *
 *  Created on: 11/03/2011
 *      Author: Anakis
 */

#include "server/zone/managers/weather/WeatherManager.h"
#include "server/zone/Zone.h"
#include "events/WeatherChangeEvent.h"
#include "server/zone/objects/creature/CreatureObject.h"
#include "templates/params/creature/CreatureAttribute.h"
#include "templates/params/creature/CreaturePosture.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "weathermaps/WeatherMap.h"
#include "server/zone/packets/scene/ServerWeatherMessage.h"

void WeatherManagerImplementation::initialize() {
	//Customize the Manager's name.
	String managerName = "WeatherManager ";
	setLoggingName(managerName + zone->getZoneName());
	setGlobalLogging(true);
	setLogging(false);

	weatherEnabled = true;
	weatherChangeEvent = nullptr;

	//Load weather configuration from the luas.
	debug() << "Loading configuration from Lua.";

	if(!loadLuaConfig()) {
		info("ERROR in Lua config. Loading default values.");
		loadDefaultValues();
	}

	windX.removeAll();
	windY.removeAll();
	windMagnitude.removeAll();

	for(int i = 0; i <= 200; ++i) {
		windX.add(((float)System::random(200) / 100.f) - 1);
		windY.add(((float)System::random(200) / 100.f) - 1);
		windMagnitude.add(((float)System::random(200) / 100.f) - 1);
	}

	createNewWeatherPattern();

	 info("Enabled");
}

bool WeatherManagerImplementation::loadLuaConfig() {
	Locker weatherManagerLocker(_this.getReferenceUnsafeStaticCast());

	Lua* lua = new Lua();
	lua->init();

	if (!lua->runFile("scripts/managers/weather_manager.lua"))
		return false;

	LuaObject luaObject = lua->getGlobalObject(zone->getZoneName());

	if (!luaObject.isValidTable())
		return false;

	//Starting weather
	baseWeather = luaObject.getByteField("defaultWeather");

	//New weather selection times.
	averageWeatherDuration = luaObject.getIntField("averageWeatherDuration");

	//Weather Stability.
	weatherStability = luaObject.getIntField("weatherStability");
	if(weatherStability > 100)
		weatherStability = 100;

	hasDamagingWeather = luaObject.getByteField("hasDamagingWeather");

	if(hasDamagingWeather) {

		weatherDamage = luaObject.getIntField("weatherDamage");
		if (weatherDamage > 500)
			return false;
	}

	//Weather type used for damaging storms
	stormType = luaObject.getByteField("stormType");

	luaObject.pop();
	delete lua;
	lua = nullptr;
	return true;
}

void WeatherManagerImplementation::loadDefaultValues() {
	Locker weatherManagerLocker(_this.getReferenceUnsafeStaticCast());

	weatherEnabled = true;

	baseWeather = 0;
	//Transition times.
	averageWeatherDuration = 3600; //seconds. 1 hour
	weatherStability = 80; // Pretty Stable

	hasDamagingWeather = false;
}

void WeatherManagerImplementation::createNewWeatherPattern() {

	Locker weatherManagerLocker(_this.getReferenceUnsafeStaticCast());

	int roll = System::random(100);

	for(int i = 0; i < 5; ++i) {
		float value = ((float)weatherStability + (i * ((100.f - (float)weatherStability) / (1.f + i))));
		value += (100.f - ((float)weatherStability + (4 * ((100.f - (float)weatherStability) / (5.f)))));

		if(roll <  value) {
			baseWeather = i;
			break;
		}
	}

	int duration = (System::random(averageWeatherDuration) + (averageWeatherDuration / 2)) / (baseWeather + 1);
	currentMap = new WeatherMap(weatherStability, zone->getMinX(), zone->getMaxX(), zone->getMinY(), zone->getMaxY(), duration);

	if(weatherChangeEvent != nullptr) {
		if(weatherChangeEvent->isScheduled())
			weatherChangeEvent->cancel();
	} else {
		weatherChangeEvent = new WeatherChangeEvent(_this.getReferenceUnsafeStaticCast());
	}

	weatherChangeEvent->reschedule(duration * 1000);
}

void WeatherManagerImplementation::sendWeatherTo(CreatureObject* player) {

	byte currentWeather = 0;

	if(currentMap != nullptr)
		currentWeather = currentMap->getWeatherAt(player->getPositionX(), player->getPositionY()) + baseWeather;
	else
		currentWeather = baseWeather;

	if(currentWeather > 4)
		currentWeather = 0;

	if(hasDamagingWeather && currentWeather == WeatherManager::EXTREMESTORM) {
		applyWeatherDamage(player);
	}

	if(player->getCurrentWeather() == currentWeather)
		return;

	if(hasDamagingWeather && player->getCurrentWeather() == WeatherManager::EXTREMESTORM) {
		player->sendSystemMessage("The winds have subsided.");
	}

	player->setCurrentWeather(currentWeather);

	if(hasDamagingWeather && currentWeather == WeatherManager::EXTREMESTORM) {
		if (stormType == WeatherManager::SAND)
			player->sendSystemMessage("The swirling sand begins to hurt you, seek shelter immediately!");
		else if (stormType == WeatherManager::COLD)
			player->sendSystemMessage("You begin to freeze in the howling winds, seek shelter immediately!");
	}

	ServerWeatherMessage* weatherMessage = new ServerWeatherMessage(
			currentWeather,
			windX.get(player->getCurrentWind()),
			windMagnitude.get(player->getCurrentWind()),
			windY.get(player->getCurrentWind()));

	player->sendMessage(weatherMessage);

}

void WeatherManagerImplementation::applyWeatherDamage(CreatureObject* player) {
	if (player == nullptr)
		return;

	PlayerObject* ghost = player->getPlayerObject();

	//Check player's online status.
	if (ghost->isTeleporting() || !player->isOnline())
		return;

	//Check if player is in a shelter.
	if ((!player->isRidingMount() && player->getParentID() != 0) || player->getCurrentCamp() != nullptr)
		return;

	//Blind player
	player->setBlindedState(15);

	//Calculate the weather protection the player has.
	float totalCoverings = calculateWeatherProtection(player);

	//Apply damage
	int applicableDamage = weatherDamage * (100.0f - totalCoverings) / 100.0f;
	if (applicableDamage > 0) {
		player->inflictDamage(nullptr, CreatureAttribute::HEALTH, applicableDamage, true, true);
		player->inflictDamage(nullptr, CreatureAttribute::ACTION, applicableDamage, true, true);
		player->inflictDamage(nullptr, CreatureAttribute::MIND, applicableDamage, true, true);
	}

	//Apply knockdown.
	if (totalCoverings < 75.0f && !player->isProne() && !player->isKnockedDown() && System::random(200) > 198 ) {
		if (player->isRidingMount()) {
			player->sendSystemMessage("A gust of wind blows you off your mount!");
			player->dismount();
		} else
			player->sendSystemMessage("A gust of wind blows you off balance!");

		player->setPosture(CreaturePosture::KNOCKEDDOWN, true);
	}
}

float WeatherManagerImplementation::calculateWeatherProtection(CreatureObject* player) {
	if (player == nullptr)
		return 0;

	float protection = 0;

	//Calculate protection for sandstorms
	if (stormType == WeatherManager::SAND) {
		const WearablesDeltaVector* clothingVector = player->getWearablesDeltaVector();
		for (int i = 0; i < clothingVector->size(); ++i) {
			TangibleObject* tano = clothingVector->get(i);
			if (tano != nullptr)
				protection += tano->getSandProtection();
		}

	//calculate protection for blizzards
	} else if (stormType == WeatherManager::COLD) {
		int playerSpecies = player->getSpecies();
		if (playerSpecies == 4 || playerSpecies == 0x32) //Wookiees and Talz get blizzard resistance automatically
			protection += 75.0f;

		const WearablesDeltaVector* clothingVector = player->getWearablesDeltaVector();
		for (int i = 0; i < clothingVector->size(); ++i) {
			TangibleObject* tano = clothingVector->get(i);
			if (tano != nullptr)
				protection += tano->getColdProtection();
		}
	}

	//max damage reduction from gear for storms is 75%
	return Math::min(protection, 75.0f);
}

void WeatherManagerImplementation::enableWeather(CreatureObject* player) {

	initialize();

	if (player != nullptr)
		player->sendSystemMessage("The weather on this planet will now change automatically.");
}

void WeatherManagerImplementation::disableWeather(CreatureObject* player) {

	if (player == nullptr || !weatherEnabled)
		return;

	Locker weatherManagerLocker(_this.getReferenceUnsafeStaticCast());
	weatherEnabled = false;

	if (weatherChangeEvent != nullptr) {
		if (weatherChangeEvent->isScheduled())
			weatherChangeEvent->cancel();

		weatherChangeEvent = nullptr;
	}

	currentMap = nullptr;

	player->sendSystemMessage("The weather on this planet will no longer change automatically.");
}

void WeatherManagerImplementation::changeWeather(CreatureObject* player, int newWeather) {

	disableWeather(player);

	Locker weatherManagerLocker(_this.getReferenceUnsafeStaticCast());

	if(newWeather == 5)
		baseWeather = System::random(5);
	else {
		baseWeather = newWeather;
	}
}

void WeatherManagerImplementation::printInfo(CreatureObject* player) {

	if(currentMap == nullptr) {
		player->sendSystemMessage("The weather currently is " + String::valueOf(baseWeather));
		return;
	}

	StringBuffer message;
	message << "Base: " << baseWeather << " " << currentMap->printInfo(player->getPositionX(), player->getPositionY());
	player->sendSystemMessage(message.toString());
}
