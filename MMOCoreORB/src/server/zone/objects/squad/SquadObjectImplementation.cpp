/*
 * SquadObjectImplementation.cpp
 *
 *  Created on: 23/4/2022
 *      Author: Halyn for EiF
 */

#include "server/zone/objects/creature/CreatureObject.h"


void SquadObjectImplementation::addMember(CreatureObject* newMember) {
	if (!squadMembers.contains(newMember)) {
		squadMembers.put(newMember);
		newMember->addToSquad(_this.getReferenceUnsafeStaticCast());
	}
}

void SquadObjectImplementation::removeMember(CreatureObject* member) {
	if (squadMembers.contains(member)) {
		squadMembers.drop(member);
		member->dropSquad();
	}
}

void SquadObjectImplementation::leaderReset() {
	CreatureObject* pLeader = getLeader();
	if (pLeader == nullptr || !squadMembers.contains(pLeader)) {
		CreatureObject* newLeader = squadMembers.get(0);
		if (newLeader != nullptr)
			leader = newLeader;
		else
			leader = nullptr;
	}
}
