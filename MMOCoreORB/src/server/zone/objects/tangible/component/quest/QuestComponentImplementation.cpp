/*
 * QuestComponentImplementation.cpp
 *
 *      Author: Halyn
 */

#include "server/zone/objects/tangible/component/quest/QuestComponent.h"

void QuestComponentImplementation::initializeTransientMembers() {
	ComponentImplementation::initializeTransientMembers();

	setLoggingName("QuestComponent");
}

void QuestComponentImplementation::notifyLoadFromDatabase() {

	TangibleObjectImplementation::notifyLoadFromDatabase();
}

int QuestComponentImplementation::getTolerance() {
	if (tolerance < 10.0f)
		return VERYCOARSE;
	else if (tolerance < 30.0f)
		return COARSE;
	else if (tolerance < 60.0f)
		return MEDIUM;
	else if (tolerance < 90.0f)
		return FINE;
	else
		return VERYFINE;
}

int QuestComponentImplementation::getPhase() {
	if (phase < 30.0f)
		return SINGLEPHASE;
	else if (phase < 55.0f)
		return TWOPHASE;
	else if (phase < 75.0f)
		return THREEPHASE;
	else
		return FOURPHASE;
}

void QuestComponentImplementation::fillAttributeList(AttributeListMessage* alm, CreatureObject* object) {
	TangibleObjectImplementation::fillAttributeList(alm, object);

	if (object == nullptr) {
		return;
	}

	String attribute;

	float value;
	double power;
	int precision;
	bool hidden;

	String footer;

	for (int i = 0; i < keyList.size(); ++i) {
		footer = "";

		attribute = keyList.get(i);
		value = attributeMap.get(attribute);
		precision = precisionMap.get(attribute);
		hidden = hiddenMap.get(attribute);

		if (precision >= 0 && !hidden) {
			if (precision >= 10) {
				footer = "%";
				precision -= 10;
			}

			StringBuffer displayvalue;

			displayvalue << Math::getPrecision(value, precision);

			displayvalue << footer;

			alm->insertAttribute(attribute, displayvalue.toString());
		}
	}

}
