/*
 * UtilityToolMenuComponent.cpp
 *
 *  Created on: 9/20/2021
 *      Author: EiF
 */

#include "server/zone/objects/creature/CreatureObject.h"
#include "server/zone/objects/player/PlayerObject.h"
#include "UtilityToolObjectMenuComponent.h"
#include "server/zone/packets/object/ObjectMenuResponse.h"

void UtilityToolObjectMenuComponent::fillObjectMenuResponse(SceneObject* sceneObject, ObjectMenuResponse* menuResponse, CreatureObject* player) const {

	if (!sceneObject->isTangibleObject())
		return;

	String text = "Open Datacard Slot";
	menuResponse->addRadialMenuItem(89, 3, text);

}

int UtilityToolObjectMenuComponent::handleObjectMenuSelect(SceneObject* sceneObject, CreatureObject* player, byte selectedID) const {

	if (!sceneObject->isTangibleObject())
		return 0;

	Reference<PlayerObject*> playObject = player->getPlayerObject();

	// Admins should be able to open.
	if (!sceneObject->isASubChildOf(player) && !playObject->isPrivileged())
		return 0;

	// Handle opening 
	if (selectedID == 89) {

		ManagedReference<SceneObject*> parent = sceneObject->getParent().get();
		if (parent != nullptr && parent->isPlayerCreature()){
			player->sendSystemMessage("You cannot change datacards while holding the tool.");
			return 0;
		}

		ManagedReference<SceneObject*> inventory = sceneObject->getSlottedObject("saber_inv");

		if (inventory == nullptr) {

			ZoneServer* zoneServer = sceneObject->getZoneServer();

			ManagedReference<SceneObject*> obj = zoneServer->createObject(
					STRING_HASHCODE("object/tangible/inventory/utility_tool_inventory.iff"), sceneObject->getPersistenceLevel());

			if (obj == nullptr)
				return 0;

			ContainerPermissions* permissions = obj->getContainerPermissionsForUpdate();
			permissions->setOwner(sceneObject->getObjectID());
			permissions->setInheritPermissionsFromParent(true);
			permissions->setDefaultDenyPermission(ContainerPermissions::MOVECONTAINER);
			permissions->setDenyPermission("owner", ContainerPermissions::MOVECONTAINER);

			if (!sceneObject->transferObject(obj, 4)) {
				obj->destroyObjectFromDatabase(true);
				return 0;
			} else {
				inventory = obj;
			}
		}


		if (inventory != nullptr) {
			inventory->sendDestroyTo(player);
			inventory->sendWithoutContainerObjectsTo(player);
			inventory->openContainerTo(player);
		} else {
			player->sendSystemMessage("Inventory object still missing!");
		}
	}

	return TangibleObjectMenuComponent::handleObjectMenuSelect(sceneObject, player, selectedID);
}
