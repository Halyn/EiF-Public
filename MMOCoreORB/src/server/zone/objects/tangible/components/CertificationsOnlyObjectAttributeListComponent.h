/*
 * CertificationsOnlyObjectAttributeListComponent.h
 *
 *  Created on: 11/22/2022
 *      Author: Klivian
 */

#ifndef CERTIFICATIONSONLYOBJECTATTRIBUTELISTCOMPONENT_H_
#define CERTIFICATIONSONLYOBJECTATTRIBUTELISTCOMPONENT_H_

#include "server/zone/objects/scene/components/AttributeListComponent.h"

class CertificationsOnlyObjectAttributeListComponent: public AttributeListComponent {
public:

	/**
	 * Fills the Attributes
	 * @pre { this object is locked }
	 * @post { this object is locked, menuResponse is complete}
	 * @param menuResponse ObjectMenuResponse that will be sent to the client
	 */
	void fillAttributeList(AttributeListMessage* alm, CreatureObject* creature, SceneObject* object) const {

		Reference<SharedTangibleObjectTemplate*> objTemplate = dynamic_cast<SharedTangibleObjectTemplate*>(object->getObjectTemplate());
		if (objTemplate == nullptr) {
			error("No SharedTangibleObjectTemplate for: " + String::valueOf(object->getServerObjectCRC()));
			return;
		}

		if(!object->isTangibleObject())
			return;

		// Already handled in tano.
		AttributeListComponent::fillAttributeList(alm, creature, object);

		/*ManagedReference<TangibleObject*> tano = cast<TangibleObject*>(object);
		if(tano->getUseCount() > 1)
			alm->insertAttribute("quantity", tano->getUseCount());*/

		//list required certification
		const auto certificationsRequired = objTemplate->getCertificationsRequired();

		for (int i = 0; i < certificationsRequired.size(); ++i) {
			const String& cert = certificationsRequired.get(i);
			String certname = "@skl_n:" + cert;
			alm->insertAttribute("weapon_cert_required", certname);
		}

		
	}

};

#endif /* CERTIFICATIONSONLYATTRIBUTELISTCOMPONENT_H_ */
