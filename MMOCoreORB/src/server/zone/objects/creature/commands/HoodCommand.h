/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef HOODCOMMAND_H_
#define HOODCOMMAND_H_

#include "server/zone/objects/creature/events/HoodTask.h"

class HoodCommand : public QueueCommand {
public:

	HoodCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->isInCombat()) {
			creature->sendSystemMessage("You're too busy fighting to worry about your hood!");
			return GENERALERROR;
		}

		Reference<Task*> pendingTask = creature->getPendingTask("hoodDown");
		if (pendingTask != nullptr)
			return SUCCESS;

		pendingTask = creature->getPendingTask("hoodUp");
		if (pendingTask != nullptr)
			return SUCCESS;

		ManagedReference<TangibleObject*> hood = creature->getSlottedObject("hood").castTo<TangibleObject*>();
		ManagedReference<TangibleObject*> overcoat = creature->getSlottedObject("chest3_r").castTo<TangibleObject*>();
		if (hood != nullptr) {
			creature->doAnimation("toggle_hood");
			Reference<Task*> task = new HoodTask(creature, hood);
			creature->addPendingTask("hoodDown", task, 1000);
		} else if (overcoat != nullptr) {
			if (!overcoat->isHoodedObject()) {
				creature->sendSystemMessage("You don't have a flexible hood!");
				return SUCCESS;
			}
			creature->doAnimation("toggle_hood");
			Reference<Task*> task = new HoodTask(creature, overcoat);
			creature->addPendingTask("hoodUp", task, 1000);
		}
		
		return SUCCESS;
	}

};

#endif //HOODCOMMAND_H_
