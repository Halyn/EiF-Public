/*EiF 2020
Please do not use without permission*/

#ifndef RAPIDFIRECOMMAND_H_
#define RAPIDFIRECOMMAND_H_

#include "CombatQueueCommand.h"

class RapidFireCommand : public CombatQueueCommand {
public:

	RapidFireCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //RAPIDFIRECOMMAND_H_
