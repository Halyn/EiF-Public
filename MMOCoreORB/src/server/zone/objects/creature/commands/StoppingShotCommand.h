/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef STOPPINGSHOTCOMMAND_H_
#define STOPPINGSHOTCOMMAND_H_

#include "CombatQueueCommand.h"

class StoppingShotCommand : public CombatQueueCommand {
public:

	StoppingShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);
	
		// Add Damage if Closer
 		if ( targetObject != nullptr && targetObject->isInRange(creature, 10.0)) {
			UnicodeString args = "healthDamageMultiplier=1.25f";
			return doCombatAction(creature, target, args);
		 } else if ( targetObject != nullptr && targetObject->isInRange(creature, 24.0)) {
			UnicodeString args = "healthDamageMultiplier=1.1f";
			return doCombatAction(creature, target, args);
		 } else {
			return doCombatAction(creature, target);
		 }
	}

};

#endif //STOPPINGSHOTCOMMAND_H_
