/*
				Copyright EiF
		See file COPYING for copying conditions.*/

#ifndef OVERHEATSHOTCOMMAND_H_
#define OVERHEATSHOTCOMMAND_H_

#include "CombatQueueCommand.h"

class OverheatShotCommand : public CombatQueueCommand {
public:

	OverheatShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //OVERHEATSHOTCOMMAND_H_
