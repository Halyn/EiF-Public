/*EiF 2020
Please do not use without permission*/

#ifndef COVERFIRECOMMAND_H_
#define COVERFIRECOMMAND_H_

#include "CombatQueueCommand.h"

class CoverFireCommand : public CombatQueueCommand {
public:

	CoverFireCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //COVERFIRECOMMAND_H_
