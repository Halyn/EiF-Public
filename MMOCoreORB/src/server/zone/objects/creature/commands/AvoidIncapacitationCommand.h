/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef AVOIDINCAPACITATIONCOMMAND_H_
#define AVOIDINCAPACITATIONCOMMAND_H_

#include "JediQueueCommand.h"

class AvoidIncapacitationCommand : public JediQueueCommand {
public:

	AvoidIncapacitationCommand(const String& name, ZoneProcessServer* server)
: JediQueueCommand(name, server) {
		 buffCRC = BuffCRC::JEDI_AVOID_INCAPACITATION;
		 blockingCRCs.add(BuffCRC::JEDI_FORCE_SHIELD_1);
		 blockingCRCs.add(BuffCRC::JEDI_FORCE_ABSORB_2);
		 blockingCRCs.add(BuffCRC::JEDI_FORCE_FEEDBACK_1);
		 
		 skillMods.put("avoid_incapacitation", 1);
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!creature->checkCooldownRecovery("avoid_incap")) {
			creature->sendSystemMessage("You are still recovering and cannot use the Force to sustain yourself."); //You cannot burst run right now.
			return false;
		}

		creature->updateCooldownTimer("avoid_incap", (duration * 1000));

		return doJediSelfBuffCommand(creature);


	}

};

#endif //AVOIDINCAPACITATIONCOMMAND_H_
