/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FLAMECONE2COMMAND_H_
#define FLAMECONE2COMMAND_H_

#include "CombatQueueCommand.h"

class FlameCone2Command : public CombatQueueCommand {
public:

	FlameCone2Command(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<WeaponObject*> weapon = creature->getWeapon();

		UnicodeString args = "healthDamageMultiplier=0.50f;mindDamageMultiplier=0.5f;";

		if (!weapon->isFlameThrower()) {
			weapon = creature->getSlottedObject("bracer_lower_l").castTo<WeaponObject*>();
			if (weapon == nullptr || !weapon->isFlameThrower())
				return INVALIDWEAPON;
			else
				return doCombatAction(creature, target, args, weapon);

		}	
		else
			return doCombatAction(creature, target, args);
	}

};

#endif //FLAMECONE2COMMAND_H_
