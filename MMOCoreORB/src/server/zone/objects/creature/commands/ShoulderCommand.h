/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef SHOULDERCOMMAND_H_
#define SHOULDERCOMMAND_H_

#include "server/zone/objects/creature/events/ShoulderTask.h"

class ShoulderCommand : public QueueCommand {
public:

	ShoulderCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (creature->isInCombat()) {
			creature->sendSystemMessage("You cannot shoulder an instrument in combat!");
			return GENERALERROR;
		}
		
		Reference<Task*> pendingTask = creature->getPendingTask("shoulder");
		if (pendingTask != nullptr)
			return SUCCESS;

		pendingTask = creature->getPendingTask("unshoulder");
		if (pendingTask != nullptr)
			return SUCCESS;

		pendingTask = creature->getPendingTask("holster");
		if (pendingTask != nullptr)
			return SUCCESS;

		pendingTask = creature->getPendingTask("unholster");
		if (pendingTask != nullptr)
			return SUCCESS;
		
		pendingTask = creature->getPendingTask("stow");
		if (pendingTask != nullptr)
			return SUCCESS;

		pendingTask = creature->getPendingTask("unstow");
		if (pendingTask != nullptr)
			return SUCCESS;

		ManagedReference<TangibleObject*> heldInstrument = creature->getSlottedObject("hold_r").castTo<TangibleObject*>();
		ManagedReference<TangibleObject*> stowedInstrument = creature->getSlottedObject("holster_ent").castTo<TangibleObject*>();
		if (stowedInstrument != nullptr) {
			creature->doAnimation("draw_pack");
			Reference<Task*> task = new ShoulderTask(creature, stowedInstrument);
			creature->addPendingTask("unshoulder", task, 500);
		} else if (heldInstrument != nullptr && heldInstrument->isInstrument()) {
			creature->doAnimation("stow_pack");
			Reference<Task*> task = new ShoulderTask(creature, heldInstrument);
			creature->addPendingTask("shoulder", task, 500);
		}

		// creature->info("transfer item command");
		return SUCCESS;
	}

};

#endif //HOLSTERCOMMAND_H_
