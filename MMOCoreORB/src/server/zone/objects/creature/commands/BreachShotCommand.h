/*
				Copyright EiF
		See file COPYING for copying conditions.*/

#ifndef BREACHSHOTCOMMAND_H_
#define BREACHSHOTSHOTCOMMAND_H_

#include "CombatQueueCommand.h"

class BreachShotCommand : public CombatQueueCommand {
public:

	BreachShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //BREACHSHOTCOMMAND_H_
