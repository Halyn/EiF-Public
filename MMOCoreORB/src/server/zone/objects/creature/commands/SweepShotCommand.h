/*
				Copyright EiF
		See file COPYING for copying conditions.*/

#ifndef SWEEPSHOTCOMMAND_H_
#define SWEEPSHOTCOMMAND_H_

#include "CombatQueueCommand.h"

class SweepShotCommand : public CombatQueueCommand {
public:

	SweepShotCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //SWEEPSHOTCOMMAND_H_
