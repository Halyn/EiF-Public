/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef UtilizeCommand_H_
#define UtilizeCommand_H_

#include "server/zone/managers/director/DirectorManager.h"

class UtilizeCommand : public QueueCommand {
public:

	UtilizeCommand(const String& name, ZoneProcessServer* server)
		: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->checkCooldownRecovery("utilize"))
			return GENERALERROR;

		SceneObject* utilityObject = creature->getSlottedObject("hold_l");

		if (utilityObject == nullptr || !utilityObject->isUtilityObject()) {
			creature->sendSystemMessage("You are not holding a usable device!");
			return SUCCESS;	
		}

		SceneObject* inventory = utilityObject->getSlottedObject("saber_inv");
		if (inventory == nullptr) {
			creature->sendSystemMessage("This device has no datacard.");
			return SUCCESS;	
		}

		if (inventory->getContainerObjectsSize() == 0) {
			creature->sendSystemMessage("This device has no datacard.");
			return SUCCESS;	
		}

		TangibleObject* datacard = inventory->getContainerObject(0).castTo<TangibleObject*>();
		if (datacard == nullptr) {
			creature->sendSystemMessage("Bad object in utility device. Please report to Halyn.");
			return SUCCESS;	
		}

		String screenplayName = datacard->getLuaStringData("screenplayName");
		String screenplayFunction = datacard->getLuaStringData("screenplayFunction");

		if (screenplayName.isEmpty() || screenplayFunction.isEmpty()) {
			creature->sendSystemMessage("The datacard in this device is corrupt.");
			return SUCCESS;	
		}

		Lua* lua = DirectorManager::instance()->getLuaInstance();

		Reference<LuaFunction*> screenplayCall = lua->createFunction(screenplayName, screenplayFunction, 0);
		*screenplayCall << creature;
		*screenplayCall << utilityObject;
		*screenplayCall << datacard;

		screenplayCall->callFunction();

		creature->updateCooldownTimer("utilize", 5000);

		return SUCCESS;
	}

};

#endif //UtilizeCommand_H_
