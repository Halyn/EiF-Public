/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions. */

#ifndef FORCEMEDITATECOMMAND_H_
#define FORCEMEDITATECOMMAND_H_

#include "server/zone/objects/player/events/ForceMeditateTask.h"

class ForceMeditateCommand : public QueueCommand {
public:

	ForceMeditateCommand(const String& name, ZoneProcessServer* server)
	: QueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		if (!creature->isPlayerCreature())
			return GENERALERROR;

//		if (isWearingArmor(creature)) {
//			return NOJEDIARMOR;
//		}
		
		if (creature->isInCombat()) {
			creature->sendSystemMessage("@jedi_spam:not_while_in_combat");
			return GENERALERROR;
		}

		if (creature->isMeditating()) {
			creature->sendSystemMessage("@jedi_spam:already_in_meditative_state");
			return GENERALERROR;
		}

		// Play Client Effect once.

		creature->playEffect("clienteffect/pl_force_meditate_self.cef", "");

		// Force Meditate Task
		ManagedReference<PlayerObject*> ghost = creature->getPlayerObject();
		int forceAlignment = creature->getForceAlignment();
		if (forceAlignment < -10000){
				creature->sendSystemMessage("The Force messed something up, one moment...");
				creature->setForceAlignment(0);
		}
		else if (forceAlignment == -10000){
			creature->sendSystemMessage("Your chains are broken.");
		}
		else if (forceAlignment > -10000 && forceAlignment < -5000){
			creature->sendSystemMessage("The Force bends to your will.");
		}
		else if (forceAlignment >= -5000 && forceAlignment < -2500){
			creature->sendSystemMessage("Your hate makes you strong.");
		}
		else if (forceAlignment >= -2500 && forceAlignment < -100){
			creature->sendSystemMessage("Your anger gives you strength.");
		}
		else if (forceAlignment >= -100 && forceAlignment < 100){
			creature->sendSystemMessage("You feel balance within the Force.");
		}
		else if (forceAlignment >= 100 && forceAlignment < 2500){
			creature->sendSystemMessage("The Force is strong with you.");
		}
		else if (forceAlignment >= 2500 && forceAlignment < 5000 ){
			creature->sendSystemMessage("You feel the Force flowing through you.");
		}
		else if (forceAlignment >= 5000 && forceAlignment < 10000 ){
			creature->sendSystemMessage("The Force is your ally.");
		}
		else if (forceAlignment == 10000 ){
			creature->sendSystemMessage("You are one with the Force.");
		} else {
			creature->sendSystemMessage("You have become more powerful than any Jedi...cheater, Abi is coming for you....");
			creature->setForceAlignment(0);
		}


		creature->sendSystemMessage("@teraskasi:med_begin");
		Reference<ForceMeditateTask*> fmeditateTask = new ForceMeditateTask(creature);
		fmeditateTask->setMoodString(creature->getMoodString());
		creature->addPendingTask("forcemeditate", fmeditateTask, 3500);

		creature->setMeditateState();

		PlayerManager* playermgr = server->getZoneServer()->getPlayerManager();	
		creature->registerObserver(ObserverEventType::POSTURECHANGED, playermgr);
		return SUCCESS;

	}

};

#endif //FORCEMEDITATECOMMAND_H_
