/*
				Copyright <SWGEmu>
		See file COPYING for copying conditions.*/

#ifndef FORCECHOKECOMMAND_H_
#define FORCECHOKECOMMAND_H_

#include "server/zone/objects/creature/commands/effect/CommandEffect.h"
#include "ForcePowersQueueCommand.h"

class ForceChokeCommand : public ForcePowersQueueCommand {
public:

	ForceChokeCommand(const String& name, ZoneProcessServer* server)
		: ForcePowersQueueCommand(name, server) {

	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

//		if (isWearingArmor(creature)) {
//			return NOJEDIARMOR;
//		}

		if ( creature->getHAM(6) < 2000 ) {
			creature->sendSystemMessage("You do not have the mental focus required for this force ability");
			return INVALIDPARAMETERS;
		}

		ManagedReference<SceneObject*> targetObject = server->getZoneServer()->getObject(target);

		if (targetObject == nullptr || !targetObject->isCreatureObject()) {
			return INVALIDTARGET;
		}

		CreatureObject *tarCreo = targetObject->asCreatureObject();

		if (tarCreo->getDamageOverTimeList()->hasDot(CommandEffect::FORCECHOKE))
		{
			creature->sendSystemMessage("Your target is already being choked"); //You cannot burst run right now.
			return INVALIDTARGET;
		}

		int res = doCombatAction(creature, target);
		if (res == SUCCESS) {
			int forceAlignment = creature->getForceAlignment();
			forceAlignment -= 1;
			creature->setForceAlignment(forceAlignment);
		}
		return res;

	}

};

#endif //FORCECHOKECOMMAND_H_
