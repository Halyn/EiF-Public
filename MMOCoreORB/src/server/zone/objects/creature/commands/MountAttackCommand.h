/*
				Copyright Empire in Flames project
*/

#ifndef MOUNTATTACKCOMMAND_H_
#define MOUNTATTACKCOMMAND_H_

#include "CombatQueueCommand.h"

class MountAttackCommand : public CombatQueueCommand {
public:

	MountAttackCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	float getCommandDuration(CreatureObject* object, const UnicodeString& arguments) const {
		return 2.0;
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {
		ZoneServer* zoneServer = server->getZoneServer();

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		ManagedReference<SceneObject*> object = zoneServer->getObject(target);

		if (object == nullptr)
			return INVALIDTARGET;

		if (!creature->isFacingObject(object))
			return INVALIDTARGET;

		if (creature->isRidingMount()){
			ManagedReference<CreatureObject*> mount = creature->getParent().get().castTo<CreatureObject*>();
			if (mount != nullptr && mount->isPet() && object->isInRange(mount, 10.0))
			{
				creature->setCombatState();
				int originalMask = mount->getPvpStatusBitmask();
				mount->setPvpStatusBitmask(originalMask + CreatureFlag::COMBATVEHICLE);
				Locker crossLocker(mount, creature);
				bool result = doCombatAction(mount, target);
				mount->setPvpStatusBitmask(originalMask);
				return result;
			}
		}
		return INVALIDTARGET;
	}
};

#endif //FIRECANNONSCOMMAND_H_
