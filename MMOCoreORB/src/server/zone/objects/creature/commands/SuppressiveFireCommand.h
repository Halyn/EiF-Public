/*EiF 2020
Please do not use without permission*/

#ifndef SUPPRESSIVEFIRECOMMAND_H_
#define SUPPRESSIVEFIRECOMMAND_H_

#include "CombatQueueCommand.h"

class SuppressiveFireCommand : public CombatQueueCommand {
public:

	SuppressiveFireCommand(const String& name, ZoneProcessServer* server)
		: CombatQueueCommand(name, server) {
	}

	int doQueueCommand(CreatureObject* creature, const uint64& target, const UnicodeString& arguments) const {

		if (!checkStateMask(creature))
			return INVALIDSTATE;

		if (!checkInvalidLocomotions(creature))
			return INVALIDLOCOMOTION;

		return doCombatAction(creature, target);
	}

};

#endif //SUPPRESSIVEFIRECOMMAND_H_
